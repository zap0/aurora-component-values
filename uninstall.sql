UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (x0.2)', '') WHERE SDComponentID = 3;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Small' WHERE SDComponentID = 3 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (10k)', '') WHERE SDComponentID = 479;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (1kt)', '') WHERE SDComponentID = 26276;
UPDATE FCT_ShipDesignComponents SET Name = 'Hangar Deck' WHERE SDComponentID = 26276 AND Name = 'Hangar Deck';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (250t)', '') WHERE SDComponentID = 33433;
UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (x1)', '') WHERE SDComponentID = 43528;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Standard' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (200)', '') WHERE SDComponentID = 43532;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport - Emergency' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (1k)', '') WHERE SDComponentID = 43533;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport - Small' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (5kt)', '') WHERE SDComponentID = 55437;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Large' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (1kt)', '') WHERE SDComponentID = 55438;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Standard' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (50t)', '') WHERE SDComponentID = 62489;
UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay - Small' WHERE SDComponentID = 62489 AND Name = 'Boat Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (x0.02)', '') WHERE SDComponentID = 65307;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Tiny' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (1kt)', '') WHERE SDComponentID = 65454;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Standard' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (250t)', '') WHERE SDComponentID = 65848;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Small' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (100t)', '') WHERE SDComponentID = 65850;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Very Small' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (x5)', '') WHERE SDComponentID = 67059;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Large' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (100t)', '') WHERE SDComponentID = 67060;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Very Small' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' (250t)', '') WHERE SDComponentID = 78586;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Small' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay';
