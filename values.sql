UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 3 AND Name = 'Cargo Hold - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (x0.2)' WHERE SDComponentID = 3 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (x0.2)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (10k)' WHERE SDComponentID = 479 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (10k)';

UPDATE FCT_ShipDesignComponents SET Name = 'Hangar Deck' WHERE SDComponentID = 26276 AND Name = 'Hangar Deck';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 26276 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (1kt)';

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 33433 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (250t)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (x1)' WHERE SDComponentID = 43528 AND NOT substr(Name, length(Name) - 4, length(Name)) LIKE ' (x1)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport - Emergency';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (200)' WHERE SDComponentID = 43532 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (200)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1k)' WHERE SDComponentID = 43533 AND NOT substr(Name, length(Name) - 4, length(Name)) LIKE ' (1k)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5kt)' WHERE SDComponentID = 55437 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (1kt)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 55438 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (1kt)';

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 62489 AND Name = 'Boat Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 62489 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (50t)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (x0.02)' WHERE SDComponentID = 65307 AND NOT substr(Name, length(Name) - 7, length(Name)) LIKE ' (x0.02)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 65454 AND NOT substr(Name, length(Name) - 5, length(Name)) LIKE ' (1kt)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 65848 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (250t)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (100t)' WHERE SDComponentID = 65850 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (100t)';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (x5)' WHERE SDComponentID = 67059 AND NOT substr(Name, length(Name) - 4, length(Name)) LIKE ' (x5)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (100t)' WHERE SDComponentID = 67060 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (100t)';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 78586 AND NOT substr(Name, length(Name) - 6, length(Name)) LIKE ' (250t)';
