# Standard Components Renaming (Values) #

This mod for C# Aurora4X is part of a series of modular standard component renaming mods. See other repos here: [Sorting](https://gitlab.com/db48x/aurora-component-sorter-2), [Sizes](https://gitlab.com/db48x/aurora-component-sizes)

----

This mod adds relevant values of a given module to it's name. Can't remember if a Small Cargo Hold was a quarter or a fifth of a regular Cargo Hold? This mod is for you.

### Examples ###

* Cargo Hold - Small -> Cargo Hold - Small (x0.2)
* Boat Bay - Small -> Boat Bay (50t)
* Cryogenic Transport -> Cryogenic Transport (10k)
* Cryogenic Transport - Emergency -> Cryogenic Transport (200)
* Cryogenic Transport - Small -> Cryogenic Transport (1k)
* Troop Transport Boarding Bay - Small -> Troop Transport Boarding Bay (250t)

Note that the sizes in tons given for these components are the capacity, not the size of the component itself. Hangars and Troop Drop/Boarding components are a little larger than their capacity.
If you also use the [Standard Component Sizes Mod](https://gitlab.com/db48x/aurora-component-sizes) you will see both the component size and the capacity.

### Compatibility ###

Explicitly compatible with the other standard component renaming mods linked at the start of this file. Will handle uninstalls gracefully (so you can see what you want or don't want).
There was a breaking change in Aurora 1.13, and consequently there are two versions of the mod available, the current one for 1.13 (likely also compatibile with subsequent versions), and one for 1.0 to 1.12.
It can be added to ongoing games (or removed from one later) and will apply to all games saved in that DB.

Not compatible with any other mods that change the names of the components in question. Will also not work for any newly added components, say an XL Cargo Hold with 10 times standard capacity, as it looks specifically for the vanilla components.

### Install ###

Available through [Aurora Loader](https://github.com/Aurora-Modders/AuroraLoader/releases).
Alternatively you can run the sql file on the Aurora DB with a tool of your choice.
